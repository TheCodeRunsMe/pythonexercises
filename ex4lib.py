# File ex4lib to import fence and outer functions

def fence(str1, str2):
    """
    returns joined str1, str2, and str1 with "_".

    Example:
    fence("aaa", "bbb")
    """
    return "_".join([str1, str2, str1])

def outer(string):
    """
    return the first and last character joined

    Example:
    outer("betis")
    """
    return string[0] + string[-1]
